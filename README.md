Prueba global logic

Requerimientos:
- JAVA 8
- Gradle 6.0.1
- Maven

******************************************************
Comandos importantes:

- gradlew bootJar: Compila un archivo jar ejecutable que contiene las clases principales y sus dependencias.

- gradlew build: Compila y prueba este proyecto.

- gradlew bootRun: Arranca el proyecto como una aplicación Springboot.

*******************************************************
Base de datos
localhost:8080/h2 - ingresar a cliente de base de datos.

url de base de datos=jdbc:h2:mem:testdb
username=sa
password=password
database-platform=org.hibernate.dialect.H2Dialect

********************************************************
Endpoint de servicio:

localhost:8080/user POST - Creación de usuario.

JSON de prueba: "{
	"name": "Juan Rodriguez",
	"email": "juadFn@assdcA.casSe",
	"password": "12ammaAA",
	"phones": [
		{
			"number": "1234567",
			"citycode": "1",
			"contrycode": "57"
		}
	]
}"
