package com.globallogic.component.errors;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorInfo {

   @JsonProperty("message")
   private String message;

   public ErrorInfo(String message) {
       this.message = message;

   }

   public String getMessage() {
       return message;
   }
}
