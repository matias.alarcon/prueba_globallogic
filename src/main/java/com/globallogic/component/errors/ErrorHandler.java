package com.globallogic.component.errors;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {
	private static Log log = LogFactory.getLog(ErrorHandler.class);
	
	@ExceptionHandler(javax.validation.ConstraintViolationException.class)
	public ResponseEntity<ErrorInfo> Exception(HttpServletRequest request, javax.validation.ConstraintViolationException e) {
        Set<ConstraintViolation<?>> fieldErrors = e.getConstraintViolations();
        StringBuilder errorMessage = new StringBuilder();
        fieldErrors.forEach(f -> errorMessage.append(f.getMessage()));
		ErrorInfo errorInfo = new ErrorInfo(errorMessage.toString());		
		
		log.error(errorInfo.getMessage());
		return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorInfo> ConstraintViolationException(HttpServletRequest request, ConstraintViolationException e) {
		ErrorInfo errorInfo = new ErrorInfo("El correo ya está registrado");
		
		log.error(errorInfo.getMessage());
		return new ResponseEntity<>(errorInfo, HttpStatus.CONFLICT);
	}
}
