package com.globallogic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.globallogic.DTO.UserRequestDTO;
import com.globallogic.DTO.UserResponseDTO;
import com.globallogic.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/user",method = RequestMethod.POST)
	public ResponseEntity<Object> createUser(@RequestBody @Valid UserRequestDTO user) {
		UserResponseDTO urDTO = userService.save(user);
		return new ResponseEntity<Object>(urDTO, HttpStatus.CREATED);
	}
}
