package com.globallogic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.globallogic.DTO.TelephoneDTO;

@Entity
@Table
public class Telephone {

	@Id
	@Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTelephone;

	private String number;
	private String citycode;
	private String contrycode;
	
	@ManyToOne
	@JoinColumn(name="idUser")
	private User user;

	public Telephone() {
		super();
	}

	public Telephone(Long id, String number, String citycode, String contrycode, User user) {
		super();
		this.idTelephone = id;
		this.number = number;
		this.citycode = citycode;
		this.contrycode = contrycode;
		this.user = user;
	}

	public Telephone(TelephoneDTO telephoneDTO) {
		this.number=telephoneDTO.getNumber();
		this.citycode = telephoneDTO.getCitycode();
		this.contrycode = telephoneDTO.getContrycode();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getContrycode() {
		return contrycode;
	}

	public void setContrycode(String contrycode) {
		this.contrycode = contrycode;
	}	
	
	public Long getId() {
		return idTelephone;
	}

	public void setId(Long id) {
		this.idTelephone = id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
