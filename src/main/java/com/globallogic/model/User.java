package com.globallogic.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.globallogic.DTO.TelephoneDTO;
import com.globallogic.DTO.UserRequestDTO;

@Entity
@Table(name="user")
@EntityListeners(AuditingEntityListener.class)
public class User {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column
	private UUID idUser;
	
	private String name; 
	
	@Column(unique=true)
	@Pattern(regexp="([a-z|A-Z0-9_\\.-]+)@([\\da-z|A-Z-]+)\\.([a-z|A-Z]{2,6})$",message="Favor seguir el formato correcto de un correo. Ejemplo: xxxxxxx@dominio.cl.")
	private String email;
	
	@Pattern(regexp="(?=(.*[a-z])).*(?=(.*\\d){2}).*(?=(.*[A-Z]){1}).*",message="La contraseña debe tener al menos una mayuscula, letras minusculas y 2 n�meros.")
	private String password;

	private String token;
	private Boolean isActive;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private	List<Telephone> phones;
	
	private Date created;
	
	@LastModifiedDate
	private Date modified;
	
	@Column(name="last_login")
	private Date lastLogin;

	public User() {
	}

	public User(UUID id, String name, String email, String password, String token, Boolean isActive,
			List<Telephone> phones, Date created, Date modified, Date lastLogin) {
		super();
		this.idUser = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.token = token;
		this.isActive = isActive;
		this.phones = phones;
		this.created = created;
		this.modified = modified;
		this.lastLogin = lastLogin;
	}
	
	public User(UserRequestDTO userRequestDTO) {
		this.name = userRequestDTO.getName();
		this.email = userRequestDTO.getEmail();
		this.password = userRequestDTO.getPassword();
		this.phones = new ArrayList<Telephone>();
		
		for(TelephoneDTO telephoneDTO: userRequestDTO.getPhones()) {
			this.phones.add(new Telephone(telephoneDTO));
		}
	}

	public UUID getId() {
		return idUser;
	}

	public void setId(UUID id) {
		this.idUser = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public List<Telephone> getPhones() {
		return phones;
	}

	public void setPhones(List<Telephone> phones) {
		this.phones = phones;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}
	
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", name=" + name + ", email=" + email + ", password=" + password + ", token="
				+ token + ", isActive=" + isActive + ", phones=" + phones + ", created=" + created + ", modified="
				+ modified + ", lastLogin=" + lastLogin + "]";
	}
}
