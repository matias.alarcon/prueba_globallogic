package com.globallogic.DTO;

public class TelephoneDTO {
	private String number;
	private String citycode;
	private String contrycode;

	public TelephoneDTO() {
		super();
	}

	public TelephoneDTO(String number, String citycode, String contrycode) {
		super();
		this.number = number;
		this.citycode = citycode;
		this.contrycode = contrycode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getContrycode() {
		return contrycode;
	}

	public void setCountrycode(String countrycode) {
		this.contrycode = countrycode;
	}	
}
