package com.globallogic.DTO;

import java.util.List;

public class UserRequestDTO {
	private String name; 
	private String email;
	private String password;
	private	List<TelephoneDTO> phones;

	public UserRequestDTO() {}

	public UserRequestDTO(String name, String email, String password, List<TelephoneDTO> phones) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.phones = phones;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<TelephoneDTO> getPhones() {
		return phones;
	}
	public void setPhones(List<TelephoneDTO> phones) {
		this.phones = phones;
	}
}
