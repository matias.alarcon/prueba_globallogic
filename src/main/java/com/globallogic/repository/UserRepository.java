package com.globallogic.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.globallogic.model.User;

public interface UserRepository extends JpaRepository<User, UUID> {
}
