package com.globallogic.service;

import org.springframework.stereotype.Service;

import com.globallogic.DTO.UserRequestDTO;
import com.globallogic.DTO.UserResponseDTO;

@Service
public interface UserService {
	
	public UserResponseDTO save(UserRequestDTO user);

}
