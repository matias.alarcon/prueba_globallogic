package com.globallogic.service;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.DTO.UserRequestDTO;
import com.globallogic.DTO.UserResponseDTO;
import com.globallogic.component.JwtTokenUtil;
import com.globallogic.model.User;
import com.globallogic.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	private static Log log = LogFactory.getLog(User.class);
	
	@Autowired
	private UserRepository userRep;
	
	@Autowired
	private JwtTokenUtil jwt;
	
	public UserResponseDTO save(UserRequestDTO userDTO) {
		User userEntity = new User(userDTO);
		Date dateCreationLastLogin = new Date();
		
		log.info("Configurando datos iniciales de usuario.10");
		userEntity.setCreated(dateCreationLastLogin);
		userEntity.setLastLogin(dateCreationLastLogin);
		userEntity.setIsActive(true);
		
		userEntity.setToken(jwt.generateToken(userEntity));
		
		userRep.save(userEntity);
		
		log.info("Usuario guardado exitosamente. Datos de usuario:");
		log.info(userEntity.toString());
		
		UserResponseDTO userResponse = new UserResponseDTO(userEntity.getId(), 
												   userEntity.getCreated(), 
												   userEntity.getModified(), 
												   userEntity.getLastLogin(), 
												   userEntity.getToken(), 
												   userEntity.getIsActive());
		return userResponse;
	}
}
