package com.globallogic;

import static org.junit.Assert.*
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.RETURNS_DEFAULTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

import com.globallogic.DTO.TelephoneDTO
import com.globallogic.DTO.UserRequestDTO
import com.globallogic.controller.UserController;
import com.globallogic.service.UserService

import spock.lang.Specification;

@SpringBootTest
class CreateUserTests extends Specification {

	@Autowired
	private UserService userService;
	
	@Test
	def "podría no crear"() {
		setup:
		List<TelephoneDTO> phones = new ArrayList()
		TelephoneDTO phone = new TelephoneDTO("1234567", "1", "57")
		phones.add(phone)
		UserRequestDTO user = new UserRequestDTO("Juan Rodriguez", 
												 "juan@rodriguez.org",
												 "hunter2",
												 phones)
		
		when:
		userService.save(user)
		
		then:
		notThrown(Exception)
	}
	

//	@Test
//	def "podria no crear el usuario correctamente"() {
//		given: 
//		String jsonValues = '"name": "Juan Rodriguez", '+
//							'"email": "juan@rodriguez.org",'+ 
//							'"password": "hunter2",'+
//							'"phones": ["'+
//								'{"number": "1234567","citycode": "1","contrycode": "57"}]}'
//									
//		when:
//		def results = mvc.perform(post('/user').contentType(APPLICATION_JSON)
//						 .content(jsonValues))
//		
//		then:
//		results.andExpect(status().isCreated())
//	}
	
//	def "Seg�n parametros de requerimiento, simular la creaci�n de J. Rodriguez"() {
//		expect: 
//		mvc.perform(
//			MockMvcRequestBuilders.post("/user")
//			.contentType(MediaType.APPLICATION_JSON)
//			.content('{"name": "Juan Rodriguez", "email": "juan@rodriguez.org", "password": "hunter2", "phones": [{"number": "1234567","citycode": "1","contrycode": "57"}]}')
//			.andExpect(status().isCreated())
//		)			
//	}
}
